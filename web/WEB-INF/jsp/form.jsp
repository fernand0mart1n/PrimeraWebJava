<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <c:if test="${not empty errores}">
            <div class="error">
                Error al procesar el formulario.
                <ul>
                    <c:forEach var="item" items="${errores}">
                        <li>${item}</li>
                    </c:forEach>
                </ul>
            </div>
        </c:if>
        <form action="/procesar" method="post">
            Nombre: <input name="nombre">
            Edad: <input name="edad">
            Lugar: <select>
                <c:forEach var="pais" items="${lugar}">
                    <option value="${pais}">${pais}</option>
                </c:forEach>
            </select>
        </form>
    </body>
</html>
