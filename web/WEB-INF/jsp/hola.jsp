<%-- 
    Document   : index
    Created on : 12/08/2016, 15:00:34
    Author     : universidad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Bienvenida</h1>
        
        <c:if test="${not empty nombre}">
            <p>Hola, ${nombre}</p>
        </c:if>

        <c:if test="${empty nombre}">
            <p>Hola, DESCONOCIDO</p>
        </c:if>
    </body>
</html>
