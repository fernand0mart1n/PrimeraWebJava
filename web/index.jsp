<%-- 
    Document   : index
    Created on : 12/08/2016, 15:00:34
    Author     : universidad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Primera pagina JSP</h1>
        <p>Acá pondremos el contenido</p>
        <%--Estos son scriplets --%>
        <% String palabra = "Hola"; %>
        <%= palabra %>
    </body>
</html>
